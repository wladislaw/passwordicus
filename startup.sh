#!/bin/bash
# 
# startup for use from init scripts
# (c) author Wladislaw (infinitylx@infinitylx.org.ua)
PID=/tmp/passwordicus.pid  #TODO: move it to /var/run/...
nohup gunicorn -w 4 -b 127.0.0.1:4001 passwordicus:app --pid=$PID