#!/usr/bin/python
# coding: utf-8
#
# author Wladislaw [infinitylx(at)infinitylx(dot)org(dot)ua]
#
import random


class Symbol(object):
    """class Symbol contains all symbols separeted by hands.
    every 3 symbol is upper case
    every 5 symbol is special
    """

    llower = []
    lupper = []
    rlower = []
    rupper = []
    lnumbers = []
    rnumbers = []
    lspecial = []
    rspecial = []
    count = 0
    palphabet = {}

    def __init__(self):
        self.llower.extend(['q', 'w', 'e', 'r', 't', 's', 'd', 'f', 'g', 'a',
                            'z', 'x', 'c', 'v', 'b'])
        self.lupper.extend(['Q', 'W', 'E', 'R', 'T', 'S', 'D', 'F', 'G', 'A',
                            'Z', 'X', 'C', 'V', 'B'])
        self.rlower.extend(['y', 'u', 'i', 'o', 'p', '[', ']', 'h', 'j', 'k',
                            'l', ';', '\'', 'n', 'm', ',', '.', '/'])  # TODO: remove special symbols
        self.rupper.extend(['Y', 'U', 'I', 'O', 'P', '[', ']', 'H', 'J', 'K',
                            'L', ';', '\'', 'N', 'M', ',', '.', '/'])  # TODO: remove special symbols
        # i think that 5 is for left hand.
        # TODO: move it to settings
        self.lnumbers.extend(['1', '2', '3', '4', '5'])
        self.rnumbers.extend(['6', '7', '8', '9', '0'])
        # what to do with usual special symbols and with shift+letter?
        self.lspecial.extend(['~', '!', '@', '#', '$', '%', '`'])
        self.rspecial.extend(['^', '&', '*', '(', ')', '-', '_', '=', '+'])

        # Letters
        self.palphabet['a'] = "Alpha"
        self.palphabet['b'] = "Bravo"
        self.palphabet['c'] = "Charlie"
        self.palphabet['d'] = "Delta"
        self.palphabet['e'] = "Echo"
        self.palphabet['f'] = "Foxtrot"
        self.palphabet['g'] = "Golf"
        self.palphabet['h'] = "Hotel"
        self.palphabet['i'] = "India"
        self.palphabet['j'] = "Juliett"
        self.palphabet['k'] = "Kilo"
        self.palphabet['l'] = "Lima"
        self.palphabet['m'] = "Mike"
        self.palphabet['n'] = "November"
        self.palphabet['o'] = "Oscar"
        self.palphabet['p'] = "Papa"
        self.palphabet['q'] = "Quebec"
        self.palphabet['r'] = "Romeo"
        self.palphabet['s'] = "Sierra"
        self.palphabet['t'] = "Tango"
        self.palphabet['u'] = "Uniform"
        self.palphabet['v'] = "Victor"
        self.palphabet['w'] = "Whiskey"
        self.palphabet['x'] = "X-ray"
        self.palphabet['y'] = "Yankee"
        self.palphabet['z'] = "Zulu"
        # Numbers
        self.palphabet['0'] = "Zero"
        self.palphabet['1'] = "One"
        self.palphabet['2'] = "Two"
        self.palphabet['3'] = "Three"
        self.palphabet['4'] = "Four"
        self.palphabet['5'] = "Five"
        self.palphabet['6'] = "Six"
        self.palphabet['7'] = "Seven"
        self.palphabet['8'] = "Eight"
        self.palphabet['9'] = "Nine"
        # Special symbols
        self.palphabet['!'] = "Exclamation"
        self.palphabet['$'] = "Dollar"
        self.palphabet['?'] = "Question"
        self.palphabet['+'] = "Plus"
        self.palphabet['*'] = "Asterisk"
        self.palphabet['-'] = "Dash"
        self.palphabet['@'] = "At"
        self.palphabet['&'] = "Ampersand"
        self.palphabet['_'] = "Underscore"
        self.palphabet['='] = "Equals"
        self.palphabet['#'] = "Hash"
        self.palphabet['~'] = "Tilde"
        self.palphabet['`'] = "Grave"
        self.palphabet['^'] = "Circumflex"
        self.palphabet['%'] = "Percents"

    # TODO: add more random here. not only divmod.
    # TODO: replace divmod with separate functions with more random

    def _is_upper(self):
        '''docstring for _is_upper'''
        pass

    def _is_special(self):
        '''docstring for _is_special'''
        pass

    def _is_lower(self):
        '''docstring for _is_lower'''
        pass

    def _is_number(self):
        '''docstring'''
        pass

    def get_symbol(self):
        """Return random symbol for both hands

        every second symbol is for right hand
        every third symbol is in upper case
        every fifth symbol is special char
        """

        symbol = ""

        if divmod(self.count, 2)[1] == 0:
            # TODO: move settings about what ... to settings
            # evry third letter in uppercase
            if divmod(self.count, 3)[1] == 0:
                symbol = self.lupper[random.randint(0, len(self.lupper)-1)]
                self.count += 1

            # every five symbol is special
            elif divmod(self.count, 5)[1] == 0:
                symbol = self.lspecial[random.randint(0, len(self.lspecial)-1)]
                self.count += 1
            else:
                symbol = self.llower[random.randint(0, len(self.llower)-1)]
                self.count += 1
        else:
            # evry third letter in uppercase
            if divmod(self.count, 3)[1] == 0:
                symbol = self.rupper[random.randint(0, len(self.rupper)-1)]
                self.count += 1
            # every five symbol is special
            elif divmod(self.count, 5)[1] == 0:
                symbol = self.rspecial[random.randint(0, len(self.rspecial)-1)]
                self.count += 1
            else:
                symbol = self.rlower[random.randint(0, len(self.rlower)-1)]
                self.count += 1

        return symbol

    def get_phonetic(self, symbol):
    	"""Return phonetic word for given symbol"""

    	symbol = symbol[0]
        # Not all symbols have phonetic for now
        # TODO: add all symbols to phonetic
        return self.palphabet.get(symbol, '_')

    def clear_count(self):
        self.count = 0


class Parol(object):

    # TODO: add key to control numbers and special chars in pass.
    def __init__(self, quantity=1, size=13):
        self.smbls = Symbol()
        self.quantity = quantity
        self.size = size

    def _gen_pass(self):
        res = []

        for j in range(0, self.quantity):
            password = []

            for i in range(0, self.size):
                password.append(self.smbls.get_symbol())

            self.smbls.clear_count()
            pw = []

            for s in password:
                pw.append(self.smbls.get_phonetic(s))

            yield ''.join(password) + ' | ' + ' - '.join(pw) + ' |'

    def get_pass(self):
        res = []

        for p in self._gen_pass():
            res.append(p)

        return res


if __name__ == '__main__':
    print '\nCLI interface comming later...'
    print '\n\tfor more details please visit http://infinitylx.org/\n'