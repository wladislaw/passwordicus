# coding: utf-8
from flask import Flask, request, render_template
from gen import Parol
from logging.handlers import RotatingFileHandler
import os


PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__)
app.config.from_object(__name__)
app.debug = True

if not app.debug:
    file_handler = RotatingFileHandler(
                    os.path.join(PROJECT_PATH, 'passwordicus.log'),
                    mode='a', maxBytes=1024000, backupCount=5,
                    encoding='UTF-8')
    file_handler.setLevel(logging.ERRORE)
    app.logger.addHandler(file_handler)


@app.route('/')
def show_welcome():
    pregen = Parol(3, 13)
    pp = pregen.get_pass()

    return render_template('core.html', passwords=pp)


@app.route('/generate', methods=['POST'])
def show_results():
    # TODO: fix Parol to use all of provided settings.
    try:
        q = int(request.form['quantity'])
        l = int(request.form['lenght'])
        n = request.form['numbers']
        s = request.form['special']
    except KeyError:
        app.logger.error('No data in request.form')
    else:
        q = 5
        l = 13
        n = True
        s = False

    return render_template('show_results.html', passwords=Parol(q, l).get_pass())


@app.route('/about')
def show_about():

    return render_template('show_about.html')


if __name__ == '__main__':
    app.run()
